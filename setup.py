import codecs
import os
import re
from setuptools import setup, find_packages


def read(*parts):
    return codecs.open(os.path.join(os.path.abspath(os.path.dirname(__file__)), *parts), 'r').read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

long_description = """

"""

setup(name="django-mongo-utils",
      version=find_version('modjul', '__init__.py'),
      description="Utilities for working with MongoDB from Django.",
      classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Topic :: Software Development :: Build Tools',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.1',
        'Programming Language :: Python :: 3.2',
      ],
      keywords='django mongodb mongo',
      author='German Ilyin',
      author_email='germanilyin@gmail.com',
      url='http://bitbucket.org/yun_man_ger/django-mongo-utils',
      license='MIT',
      packages=find_packages(),
      zip_safe=False,
      )
