from mongotools.views import ListView, UpdateView
from django.http import Http404, HttpResponseRedirect
from .base import ExtraContextMixin
import re

ACTION_RE = re.compile(r'\w+')


class SelectedItemsListViewMixin(object):

    def get_success_url(self):
        if self.success_url:
            return self.success_url
        return self.request.get_full_path()

    def get_selected_items(self, request):
        pk_list = request.REQUEST.getlist(self.check_name)
        return (self.get_queryset().filter(pk__in=pk_list),)


class SelectedItemsUpdateViewMixin(object):
    def get_selected_items(self, request):
        return (self.get_object(), request.REQUEST.getlist(self.check_name),)


class ActionProcessView(object):
    """A view for managing multiple items in the list
    """

    def post(self, request, *args, **kwargs):
        action = request.POST.get('action', None)

        if action is None or not ACTION_RE.search(action):
            raise Http404

        do_action = getattr(self, 'on_{}'.format(action), None)
        if do_action is not None and callable(do_action):
            do_action(
                request,
                action,
                *self.get_selected_items(request))
        else:
            raise Http404

        return HttpResponseRedirect(self.get_success_url())


class BaseActionListView(ExtraContextMixin, SelectedItemsListViewMixin, ActionProcessView):
    check_name = 'check'
    success_url = None


class ActionListView(BaseActionListView, ListView):
    pass


class BaseActionUpdateView(ExtraContextMixin, SelectedItemsUpdateViewMixin, ActionProcessView):
    check_name = 'check'
    pass


class ActionUpdateView(BaseActionUpdateView, UpdateView):
    pass
