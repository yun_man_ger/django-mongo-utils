from mongotools.views import CreateView as MongoCreateView, \
    ListView as MongoListView, \
    UpdateView as MongoUpdateView, \
    DeleteView as MongoDeleteView, \
    DetailView as MongoDetailView
from .base import ExtraContextMixin
from .list import (ActionListView, ActionUpdateView)


class ListView(ExtraContextMixin, MongoListView):
    pass


class CreateView(ExtraContextMixin, MongoCreateView):
    pass


class UpdateView(ExtraContextMixin, MongoUpdateView):
    pass


class DeleteView(ExtraContextMixin, MongoDeleteView):
    pass


class DetailView(ExtraContextMixin, MongoDetailView):
    pass
