from mongoengine import connection
from django.conf import settings
from nose.tools import make_decorator


def flush_mongo():
    db = connection.get_db()
    assert db.name == 'test_' + settings.MONGODB_DATABASES['default'].get('NAME')
    for key in db.collection_names():
        if key != 'system.indexes':
            db.drop_collection(key)


def connect_to_test_db():
    connection.disconnect()
    test_name = 'test_' + settings.MONGODB_DATABASES['default'].get('NAME')
    connection.connect(test_name)


def flush_db(func):
    def new_func(*a, **kw):
        flush_mongo()
        func(*a, **kw)
    return make_decorator(func)(new_func)


def use_tdb(func):
    def new_func(*a, **kw):
        connect_to_test_db()
        flush_mongo()
        func(*a, **kw)
        flush_mongo()
    return make_decorator(func)(new_func)
