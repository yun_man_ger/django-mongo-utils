from mongoengine import connection
from django.conf import settings


def flush_mongo():
    db = connection.get_db()
    assert db.name == 'test_' + settings.MONGODB_DATABASES['default'].get('NAME')
    for key in db.collection_names():
        if key != 'system.indexes':
            db.drop_collection(key)


def connect_to_test_db():
    connection.disconnect()
    test_name = 'test_' + settings.MONGODB_DATABASES['default'].get('NAME')
    connection.connect(test_name)
